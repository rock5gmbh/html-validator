<?php

namespace App\AcVa\ValidatorBundle\Commands;

use App\AcVa\ValidatorBundle\AcVaValidating;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ValidatorCommand extends Command
{
    private $validating;
    public function __construct(AcVaValidating $validating)
    {
        $this->validating = $validating;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('acva:validate')
            ->setDescription('Validate URL with a predefined Service on the web')
            ->addArgument(
                'url',
                InputArgument::REQUIRED,
                'The URL you want to check'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $url = $input->getArgument('url');
        
        $result = $this->validating->validate($url);
        

        $output->writeln('Status: '. $result['status']['reason'].' '. $result['status']['code']);
        $output->writeln('done.' );
    }
}