<?php
namespace App\AcVa\ValidatorBundle;

use App\AcVa\ValidatorBundle\Model\ConfigManager;
use Doctrine\ORM\EntityManagerInterface;

class AcVaValidating
{
  private $config;

  private $em;

  private $repository;

  function __construct(ConfigManager $config, EntityManagerInterface $entityManager)
  {
    $this->config = $config->getConfig();
    
    $this->em = $entityManager;
    
    // TODO Errorhandling to access assoc array:
    $className = $this->config['entity'];
    $this->repository = $entityManager->getRepository($className);
  }
  public function validate($url) {

    $setField =  'set'.ucfirst($this->config['entity_field']);
    // Get the Entity name in 
    $entity = new $this->config['entity']();
    
    $entity->setUrl($url);
    $entity->setStatus('WAITING');

    $entity->$setField('[]');
    $this->em->persist($entity);
    $this->em->flush();


    // Initialize Guzzle client
    $client = new \GuzzleHttp\Client();

    // Create a POST request
    $response = $client->request('GET', $this->config['service_url'], [
      'query' => [
        'out' => 'json',
        'doc' => $url, 
      ]
    ]);

    $code = $response->getStatusCode(); // 200
    $reason = $response->getReasonPhrase(); // OK

    $result = null;
    if($code === 200) {
      $result = (string) $response->getBody();
    } else {
      $entity->setStatuss('ERROR');
      $result = json_encode( [
        'validator_status' => [
          'code' => $code,
          'reason' => $reason,
        ]
      ]);
    }

    $entity->$setField($result);
    $this->em->persist($entity);
    $this->em->flush();

    return [
      'config' => $this->config,
      'status' => [
        'code' => $code,
        'reason' => $reason,
      ],
      'result' => json_decode($result, true),
    ];
  }
}