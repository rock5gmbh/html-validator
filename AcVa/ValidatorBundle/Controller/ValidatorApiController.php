<?php

namespace App\AcVa\ValidatorBundle\Controller;

use App\AcVa\ValidatorBundle\AcVaValidating;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ValidatorApiController extends AbstractController
{
  private $validating;

  public function __construct(AcVaValidating $acvaValidating)
  {
    $this->validating = $acvaValidating;
  }

  public function index()
  {

    // TODO !!!DANGEROUS!!! harden against attacks! sql injection etc
    // TODO use symfony way!
    $url = strip_tags(isset($_GET['site']) ? $_GET['site']: ''); 

    return $this->json(
      $this->validating->validate($url)
    );
  }
}
