<?php
namespace App\AcVa\ValidatorBundle;

use App\AcVa\ValidatorBundle\DependencyInjection\AcVaValidatorExtension;
use Symfony\Component\Console\Application;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AcVaValidatorBundle extends Bundle
{
  /**
   * Overridden to allow for the custom extension alias.
   */
  public function getContainerExtension()
  {
    if (null === $this->extension) {
      $this->extension = new AcVaValidatorExtension();
    }
    return $this->extension;
  }

  public function registerCommands(Application $application)
  {
    //TODO register Command
  }
}