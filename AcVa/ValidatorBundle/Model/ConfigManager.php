<?php
namespace App\AcVa\ValidatorBundle\Model;


class ConfigManager
{
  private $config;

  public function setConfig($config)
  {
    $this->config = $config;
  }
  public function getConfig()
  {
    return $this->config;
  }
}
