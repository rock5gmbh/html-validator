<?php 
namespace App\AcVa\ValidatorBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
  public function getConfigTreeBuilder()
  {
    $treeBuilder = new TreeBuilder('acva_validator');
    $rootNode = $treeBuilder->getRootNode();

    $rootNode->children()
        ->scalarNode('service_url')->isRequired()->cannotBeEmpty()
          ->info('This value reflects the target URI of the valiodation service.')
          ->end()
        ->scalarNode('entity')->isRequired()->cannotBeEmpty()
          ->info('This value reflects the entity where the validation request should be stored.')
          ->end()
        ->scalarNode('entity_field')->isRequired()->cannotBeEmpty()
          ->info('This value reflects the entities property/field where the validation result should be stored.')
          ->end()
      ->end();

    return $treeBuilder;
  }
}