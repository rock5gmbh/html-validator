<?php
namespace App\AcVa\ValidatorBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class AcVaValidatorExtension extends Extension
{
  public function load(array $configs, ContainerBuilder $container)
  {
    $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
    $loader->load('services.xml');
    
    $configuration = $this->getConfiguration($configs, $container);
    $config = $this->processConfiguration($configuration, $configs);

    $configServiceDefintion = $container->getDefinition('acva_validator.model.config_manager');
    $configServiceDefintion->addMethodCall('setConfig', array($config));

  }

  public function getAlias()
  {
    return 'acva_validator';
  }
}