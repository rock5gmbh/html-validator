# Installation

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

This bundle uses `guzzle`.
Please make shure you have installed `guzzle`

```console
composer require guzzlehttp/guzzle
```

## Step 1: Download the Bundle

1. Download this the Bundle and the example from the Repository.
2. Copy and paste the `AcVa/` folder into your `src/` folder.
3. Add the Bundle config to your configs. Copy the yaml file from `_examples/config/packages/` into your `config/packages/`
4. Edit the confis as you need.
    > **Attention:** for this demonststration purpose, the linked Entity should have at least the fields `status` and `url` and one additional `result` (name may be changed in config) - all of type `string`.
    See `_examples/Entity/Records.php` and `_examples/Repository/RecordsRepository.php`

## Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

```php
// config/bundles.php

return [
    // ...
    App\AcVa\ValidatorBundle\AcVaValidatorBundle::class => ['all' => true],
];
```

## Known Flaws

* Getting the URL is horribly insecure currently! this has to be fixed later
    * at least some sql-/xss-/bad-code-injection preventions should be implemented!
    * a FOSRest service could be beneficial in solving all of this.
* Fix needed Fields in the entity. There should not be need of additional fields, but the congif changeable `result` field.
* Large results might overflow the database field.
* Exception-handling for the remote service could be improved.
* This is not "a real bundle"
  * `composer.json` and other important files are missing!
  * Could benefit greatly from a flex-recipie
* Due to driver issues, I wasn't fully able to test the persisting of the Entities.
* The command wont register correctly, and so is not useable. The config wont load correctly if accessed via command.
